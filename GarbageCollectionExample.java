import java.lang.Runtime;

public class GarbageCollectionExample {
    public static void main(String[] args) {
        // Get a reference to the Runtime class
        Runtime runtime = Runtime.getRuntime();

        // Run garbage collection
        runtime.gc();

        System.out.println("Garbage collection executed");
        // Perform any necessary operations after garbage collection
    }
}
